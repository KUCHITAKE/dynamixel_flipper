﻿// -*- C++ -*-
// <rtc-template block="description">
/*!
 * @file  dynamixel_flipperTest.cpp
 * @brief ${rtcParam.description} (test code)
 *
 */
// </rtc-template>

#include "dynamixel_flipperTest.h"

// Module specification
// <rtc-template block="module_spec">
#if RTM_MAJOR_VERSION >= 2
static const char* const dynamixel_flipper_spec[] =
#else
static const char* dynamixel_flipper_spec[] =
#endif
  {
    "implementation_id", "dynamixel_flipperTest",
    "type_name",         "dynamixel_flipperTest",
    "description",       "${rtcParam.description}",
    "version",           "1.0.0",
    "vendor",            "NUT Kimuralab Keitaro Takeuchi",
    "category",          "Controller",
    "activity_type",     "PERIODIC",
    "kind",              "DataFlowComponent",
    "max_instance",      "1",
    "language",          "C++",
    "lang_type",         "compile",
    ""
  };
// </rtc-template>

/*!
 * @brief constructor
 * @param manager Maneger Object
 */
dynamixel_flipperTest::dynamixel_flipperTest(RTC::Manager* manager)
    // <rtc-template block="initializer">
  : RTC::DataFlowComponentBase(manager),
    m_angle_targetOut("angle_target", m_angle_target),
    m_angle_actualIn("angle_actual", m_angle_actual)

    // </rtc-template>
{
}

/*!
 * @brief destructor
 */
dynamixel_flipperTest::~dynamixel_flipperTest()
{
}



RTC::ReturnCode_t dynamixel_flipperTest::onInitialize()
{
  // Registration: InPort/OutPort/Service
  // <rtc-template block="registration">
  // Set InPort buffers
  addInPort("angle_actual", m_angle_actualIn);
  
  // Set OutPort buffer
  addOutPort("angle_target", m_angle_targetOut);
  
  // Set service provider to Ports
  
  // Set service consumers to Ports
  
  // Set CORBA Service Ports
  
  // </rtc-template>

  // <rtc-template block="bind_config">
  // </rtc-template>
  
  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t dynamixel_flipperTest::onFinalize()
{
  return RTC::RTC_OK;
}
*/


RTC::ReturnCode_t dynamixel_flipperTest::onStartup(RTC::UniqueId /*ec_id*/)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t dynamixel_flipperTest::onShutdown(RTC::UniqueId /*ec_id*/)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t dynamixel_flipperTest::onActivated(RTC::UniqueId /*ec_id*/)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t dynamixel_flipperTest::onDeactivated(RTC::UniqueId /*ec_id*/)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t dynamixel_flipperTest::onExecute(RTC::UniqueId /*ec_id*/)
{
  return RTC::RTC_OK;
}


//RTC::ReturnCode_t dynamixel_flipperTest::onAborting(RTC::UniqueId /*ec_id*/)
//{
//  return RTC::RTC_OK;
//}


//RTC::ReturnCode_t dynamixel_flipperTest::onError(RTC::UniqueId /*ec_id*/)
//{
//  return RTC::RTC_OK;
//}


RTC::ReturnCode_t dynamixel_flipperTest::onReset(RTC::UniqueId /*ec_id*/)
{
  return RTC::RTC_OK;
}


//RTC::ReturnCode_t dynamixel_flipperTest::onStateUpdate(RTC::UniqueId /*ec_id*/)
//{
//  return RTC::RTC_OK;
//}


//RTC::ReturnCode_t dynamixel_flipperTest::onRateChanged(RTC::UniqueId /*ec_id*/)
//{
//  return RTC::RTC_OK;
//}


bool dynamixel_flipperTest::runTest()
{
    return true;
}


extern "C"
{
 
  void dynamixel_flipperTestInit(RTC::Manager* manager)
  {
    coil::Properties profile(dynamixel_flipper_spec);
    manager->registerFactory(profile,
                             RTC::Create<dynamixel_flipperTest>,
                             RTC::Delete<dynamixel_flipperTest>);
  }
  
}
