﻿// -*- C++ -*-
// <rtc-template block="description">
/*!
 * @file  dynamixel_flipper.cpp
 * @brief ${rtcParam.description}
 *
 */
// </rtc-template>

#include "dynamixel_flipper.h"

#include <cmath>

#include "dynamixel_sdk.h"

// Module specification
// <rtc-template block="module_spec">
#if RTM_MAJOR_VERSION >= 2
static const char* const dynamixel_flipper_spec[] =
#else
static const char* dynamixel_flipper_spec[] =
#endif
    {"implementation_id",
     "dynamixel_flipper",
     "type_name",
     "dynamixel_flipper",
     "description",
     "${rtcParam.description}",
     "version",
     "1.0.0",
     "vendor",
     "NUT Kimuralab Keitaro Takeuchi",
     "category",
     "Controller",
     "activity_type",
     "PERIODIC",
     "kind",
     "DataFlowComponent",
     "max_instance",
     "1",
     "language",
     "C++",
     "lang_type",
     "compile",
     ""};
// </rtc-template>

constexpr uint16_t ADDR_TOURQUE_ENABLE = 562;
constexpr uint16_t ADDR_PRESENT_POSITION = 611;
constexpr uint16_t ADDR_GOAL_POSITION = 596;
constexpr uint16_t ADDR_GOAL_VELOCITY = 600;
constexpr uint16_t ADDR_OPERATIONG_MODE = 11;
constexpr uint16_t VELOCITY_MODE = 1;
constexpr uint32_t PULSE_PER_REV = 501923;
constexpr double SCALE_FACTOR = 0.00199234;
constexpr double SPEED_FACTOR = 10.0;

/*!
 * @brief constructor
 * @param manager Maneger Object
 */
dynamixel_flipper::dynamixel_flipper(RTC::Manager* manager)
    // <rtc-template block="initializer">
    : RTC::DataFlowComponentBase(manager),
      m_angle_targetIn("angle_target", m_angle_target),
      m_angle_actualOut("angle_actual", m_angle_actual)
// </rtc-template>
{}

/*!
 * @brief destructor
 */
dynamixel_flipper::~dynamixel_flipper() {}

RTC::ReturnCode_t dynamixel_flipper::onInitialize() {
  // Registration: InPort/OutPort/Service
  // <rtc-template block="registration">
  // Set InPort buffers
  addInPort("angle_target", m_angle_targetIn);

  // Set OutPort buffer
  addOutPort("angle_actual", m_angle_actualOut);

  // Set service provider to Ports

  // Set service consumers to Ports

  // Set CORBA Service Ports

  // </rtc-template>

  // <rtc-template block="bind_config">
  // </rtc-template>


  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t dynamixel_flipper::onFinalize()
{
  return RTC::RTC_OK;
}
*/

RTC::ReturnCode_t dynamixel_flipper::onStartup(RTC::UniqueId /*ec_id*/) {
  return RTC::RTC_OK;
}

RTC::ReturnCode_t dynamixel_flipper::onShutdown(RTC::UniqueId /*ec_id*/) {
  return RTC::RTC_OK;
}

RTC::ReturnCode_t dynamixel_flipper::onActivated(RTC::UniqueId /*ec_id*/) {
  ids = {2, 3, 4, 5};
  const char* deviceName = "/dev/ttyUSB0";

  portHandler = dynamixel::PortHandler::getPortHandler(deviceName);
  packetHandler = dynamixel::PacketHandler::getPacketHandler();

  if (portHandler->openPort()) {
    std::cout << "Successfully opened the port!" << std::endl;
  } else {
    std::cout << "Failed to open the port!" << std::endl;
    return RTC::RTC_ERROR;
  }

  if (portHandler->setBaudRate(115200)) {
    std::cout << "Successfully set the baudrate!" << std::endl;
  } else {
    std::cout << "Failed to set the baudrate!" << std::endl;
    return RTC::RTC_ERROR;
  }

  for (const auto& id : ids) {
    packetHandler->write1ByteTxOnly(portHandler, id, ADDR_TOURQUE_ENABLE, 1);
    const auto result = packetHandler->ping(portHandler, id);
    std::cout << "id" << id << "ping result: " << result << std::endl;
  }

  for(const auto& id: ids) {
    packetHandler->write1ByteTxOnly(portHandler, id, ADDR_OPERATIONG_MODE, VELOCITY_MODE);
  }

  return RTC::RTC_OK;
}

RTC::ReturnCode_t dynamixel_flipper::onDeactivated(RTC::UniqueId /*ec_id*/) {
  for (const auto& id : ids) {
    packetHandler->write1ByteTxOnly(portHandler, id, ADDR_TOURQUE_ENABLE, 0);
  }
  return RTC::RTC_OK;
}

RTC::ReturnCode_t dynamixel_flipper::onExecute(RTC::UniqueId /*ec_id*/) {
  // write to dynamixel

  if (m_angle_targetIn.isNew()) {
    m_angle_targetIn.read();
    for (size_t i = 0; i < ids.size(); i++) {
      packetHandler->write4ByteTxRx(
          portHandler, ids[i], ADDR_GOAL_VELOCITY,
          m_angle_target.data[i] * SPEED_FACTOR / SCALE_FACTOR);
      // std::cout <<"target:" << ids[i] << ": " << m_angle_target.data[i] * SPEED_FACTOR / SCALE_FACTOR << std::endl;
    }
  }

  // read from dynamixel and publish
  m_angle_actual.data.length(ids.size());
  for (size_t i = 0; i < ids.size(); i++) {
    int32_t pulse;
    packetHandler->read4ByteTxRx(portHandler, ids[i], ADDR_PRESENT_POSITION,
                                 (uint32_t*)&pulse);
    const float angle = (pulse / PULSE_PER_REV) * M_PI * 2;
    m_angle_actual.data[i] = angle;

    // std::cout <<"actual:" << ids[i] << ": " << m_angle_actual.data[i] << std::endl;
  }
  m_angle_actualOut.write();

  return RTC::RTC_OK;
}

// RTC::ReturnCode_t dynamixel_flipper::onAborting(RTC::UniqueId /*ec_id*/)
//{
//   return RTC::RTC_OK;
// }

// RTC::ReturnCode_t dynamixel_flipper::onError(RTC::UniqueId /*ec_id*/)
//{
//   return RTC::RTC_OK;
// }

RTC::ReturnCode_t dynamixel_flipper::onReset(RTC::UniqueId /*ec_id*/) {
  return RTC::RTC_OK;
}

// RTC::ReturnCode_t dynamixel_flipper::onStateUpdate(RTC::UniqueId /*ec_id*/)
//{
//   return RTC::RTC_OK;
// }

// RTC::ReturnCode_t dynamixel_flipper::onRateChanged(RTC::UniqueId /*ec_id*/)
//{
//   return RTC::RTC_OK;
// }

extern "C" {

void dynamixel_flipperInit(RTC::Manager* manager) {
  coil::Properties profile(dynamixel_flipper_spec);
  manager->registerFactory(profile, RTC::Create<dynamixel_flipper>,
                           RTC::Delete<dynamixel_flipper>);
}
}
