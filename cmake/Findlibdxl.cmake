find_path(LIBDXL_INCLUDE_DIR dynamixel_sdk.h
  PATHS
    ENV LIBDXL_INCLUDE_DIR
    ${LIBDXL_INCLUDE_DIR}
    /usr/local/include
  PATH_SUFFIXES
    dynamixel_sdk
)

find_library(LIBDXL_LIBRARY
  NAMES
    dxl_x64_cpp
  PATHS
    ENV LIBDXL_LIB_DIR
    ${LIBDXL_LIB_DIR}
    /usr/local
  PATH_SUFFIXES
    lib
)

mark_as_advanced(LIBDXL_INCLUDE_DIR LIBDXL_LIBRARY)
include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(libdxl
  REQUIRED_VARS
    LIBDXL_INCLUDE_DIR
    LIBDXL_LIBRARY
)

if(libdxl_FOUND AND NOT TARGET libdxl::libdxl)
  add_library(libdxl::libdxl UNKNOWN IMPORTED)
  set_target_properties(libdxl::libdxl PROPERTIES
    IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
    IMPORTED_LOCATION "${LIBDXL_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${LIBDXL_INCLUDE_DIR}"
    )
endif()
